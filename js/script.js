$(document).ready(function() {
  //header при скролинге
  $(window).scroll(function () {
    if ($(this).scrollTop() > 20) {
      $('.main').addClass("sticky");
      $('.main').addClass("main_scroll");
      $('.main').css("height","5%");
    }
    else {
      $('.main').removeClass("sticky");
      $('.main').removeClass("main_scroll");
      $('.main').css("height" , "15%");
    }
  });
//для вызова меню
  $("#menu_open, #menu_close").click(function () {
    $("#sidebar").toggleClass("open");
  });
});
